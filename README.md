# DarkChat

As a result of taking a small Android Programming Course, I developed a couple of apps. One of them was DarkChat. A clone of some of whatsapp's features. with the ability for the user to wipe all of his conversations from the server

**Technology**

* JAVA
* XML
* Firebase

**What I Learned**

* Android Programming and Design ([Google Standards](https://design.google/resources/))

* Integration with Firebase
* Uploading to PlayStore