package darkchat.tanz.com.darkchat.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import darkchat.tanz.com.darkchat.Helper.Base64Custom;
import darkchat.tanz.com.darkchat.Helper.Preferencias;
import darkchat.tanz.com.darkchat.R;
import darkchat.tanz.com.darkchat.config.ConfiguracaoFirebase;
import darkchat.tanz.com.darkchat.model.Usuario;

public class CadastroActivity extends AppCompatActivity {

    private EditText nome;
    private EditText email;
    private EditText senha;
    private EditText senhaConfirmar;
    private Button botaoCadastrar;
    private ImageView fotoUser;
    private FloatingActionButton botaoImagem;
    private Usuario usuario;
    private Uri filePath;
    private final int PICK_IMAGE_REQUEST = 71;
    //FIREBASE
    private FirebaseStorage storage;
    private StorageReference imagesRef;
    private FirebaseAuth autenticacao;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        //FIREBASE INICIALIZAR
        storage = FirebaseStorage.getInstance();
        imagesRef = storage.getReference();

        fotoUser = findViewById(R.id.cadastroUserPicId);
        nome = findViewById(R.id.editCadastroNome);
        email = findViewById(R.id.editCadastroEmail);
        senha = findViewById(R.id.editCadastroSenha);
        senhaConfirmar = findViewById(R.id.editCadastroSenhaConfirmar);
        botaoCadastrar = findViewById(R.id.botaoCadastrarId);
        botaoImagem = findViewById(R.id.botaoAdicionarFotoId);


        botaoCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if((senha.getText().toString()).equals(senhaConfirmar.getText().toString())){
                    usuario = new Usuario();
                    usuario.setNome(nome.getText().toString());
                    usuario.setEmail(email.getText().toString().trim());
                    usuario.setSenha(senha.getText().toString());
                    uploadImagem();
                    cadastrarUsuario();
                }else{
                    Toast.makeText(CadastroActivity.this,"Passwords don't match! Nuclear reactor is now critical! Try Again",Toast.LENGTH_LONG).show();

                }
            }
        });

        botaoImagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escolherImagem();
            }
        });
    }


/////////////////////////TROCAR PARA APENAS FAZER O STORAGE NO CADASTRO. E ATÉ LÁ SÓ MANTER NA VIEW!
    private void escolherImagem(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null &&data.getData()!= null){
            filePath = data.getData();
            try{
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),filePath);
                fotoUser.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadImagem(){
        if(filePath!=null){
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading profile picture...");
            progressDialog.show();
            String identificadorUsuario = Base64Custom.codificartBase64(email.getText().toString());
            StorageReference ref = imagesRef.child("images").child("perfil").child(identificadorUsuario).child("perfilpic");
            ref.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(CadastroActivity.this,"Upload failed, Don't worry you can upload a picture later!",Toast.LENGTH_LONG).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Uploaded "+(int)progress+"%");
                }
            });
        }
    }

    private void cadastrarUsuario(){

        autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();
        autenticacao.createUserWithEmailAndPassword(
                usuario.getEmail(),
                usuario.getSenha()
        ).addOnCompleteListener(CadastroActivity.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    Toast.makeText(CadastroActivity.this, "You are now a member!", Toast.LENGTH_SHORT).show();

                    String identificadorUsuario = Base64Custom.codificartBase64(usuario.getEmail());
                    usuario.setId(identificadorUsuario);
                    usuario.salvar();

                    Preferencias preferencias = new Preferencias(CadastroActivity.this);
                    preferencias.salvarDados(identificadorUsuario);



                    abrirLoginUsuario();
                }else{
                    String erroExeption = "";
                    try{
                        throw task.getException();
                    }catch (FirebaseAuthWeakPasswordException e){
                        erroExeption = "Your password is to weak! Unless you want to be hacked use more characters including letters and numbers";
                    }catch (FirebaseAuthInvalidCredentialsException e) {
                        erroExeption = "Your e-mail is not valid, use a real one!";
                    }catch (FirebaseAuthUserCollisionException e) {
                        erroExeption = "Your e-mail is already a member! Spooky!";
                    }catch (Exception e) {
                        erroExeption = "Something's fishy. Please try again!";
                        e.printStackTrace();
                    }
                    Toast.makeText(CadastroActivity.this, erroExeption , Toast.LENGTH_LONG).show();
                }
        }
    });
}
    public void abrirLoginUsuario(){
        Intent intent = new Intent(CadastroActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
