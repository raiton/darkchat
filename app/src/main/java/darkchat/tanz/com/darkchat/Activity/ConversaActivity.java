package darkchat.tanz.com.darkchat.Activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.NotLinkException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import darkchat.tanz.com.darkchat.Helper.Preferencias;
import darkchat.tanz.com.darkchat.Helper.Base64Custom;
import darkchat.tanz.com.darkchat.R;
import darkchat.tanz.com.darkchat.adapter.MensagemAdapter;
import darkchat.tanz.com.darkchat.config.ConfiguracaoFirebase;
import darkchat.tanz.com.darkchat.model.Conversa;
import darkchat.tanz.com.darkchat.model.Mensagem;

public class ConversaActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private EditText editMensagem;
    private ImageButton btMensagem;
    private ImageButton btClip;
    private DatabaseReference firebase;
    private FirebaseStorage storage;
    private StorageReference storageRef;
    private StorageReference imagesRef;
    private Uri filePath;

    private ListView listView;
    private ArrayList<Mensagem> mensagens;
    private ArrayAdapter<Mensagem> adapter;
    private ValueEventListener valueEventListnerMensagem;
    private AdView adView;
    private int numero = 0;

    //DADOS DO DESTINATARIO
    private String nomeUsuarioDestinatario;
    private String idUsuarioDestinatario;

    //DADOS DO REMETENTE
    private String idUsuarioRemetente;
    private String nomeUsuarioRemetente;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversa);

        toolbar = (Toolbar) findViewById(R.id.tb_conversa);
        editMensagem = (EditText) findViewById(R.id.edit_mensagem);
        btMensagem = (ImageButton) findViewById(R.id.bt_enviar);
        btClip = (ImageButton) findViewById(R.id.bt_extra);
        listView = (ListView) findViewById(R.id.lv_conversas);

        //DADOS DO USUARIO LOGADO
        Preferencias preferencias = new Preferencias(ConversaActivity.this);
        idUsuarioRemetente = preferencias.getIdentificador();

        Bundle extra = getIntent().getExtras();
        if(extra != null){
            nomeUsuarioDestinatario = extra.getString("nome");
            String emailDestinatario = extra.getString("email");
            idUsuarioDestinatario = Base64Custom.codificartBase64(emailDestinatario);
        }

        //CONFIGURAR TOOLBAR
        toolbar.setTitle(nomeUsuarioDestinatario);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_left);
        setSupportActionBar(toolbar);

        //MONTA LISTVIEW E ADAPTER
        mensagens = new ArrayList<>();
        adapter = new MensagemAdapter(ConversaActivity.this, mensagens);
        listView.setAdapter(adapter);

        //RECUPERAR MENSANGES DO FIREBASE
        firebase = ConfiguracaoFirebase.getFirebase().child("mensagens").child(idUsuarioRemetente).child(idUsuarioDestinatario);

        //IMAGENS
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();
        imagesRef = storageRef.child("images");

        //CRIAR LISTNER PARA MENSAGENS
        valueEventListnerMensagem = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mensagens.clear();

                for(DataSnapshot dados: dataSnapshot.getChildren()){
                    Mensagem mensagem = dados.getValue(Mensagem.class);
                    mensagens.add(mensagem);
                }

                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        firebase.addValueEventListener(valueEventListnerMensagem);

        //ENVIAR MENSAGEM
        btMensagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String textoMensagem = editMensagem.getText().toString();
                if(textoMensagem.isEmpty()){
                    if(numero >=2) {
                        Toast.makeText(ConversaActivity.this, "I'm not wasting internet on a empty message...", Toast.LENGTH_SHORT).show();
                        numero = 0;
                    }else{
                        numero++;
                    }
                }else{
                    numero = 0;
                    Mensagem mensagem = new Mensagem();
                    mensagem.setIdUsuario(idUsuarioRemetente);
                    mensagem.setMensagem(textoMensagem);

                    //SALVAR MENSAGEM PARA O REMENTENTE
                    salvarMensagem(idUsuarioRemetente,idUsuarioDestinatario,mensagem);

                    //SALVAR MENSAGEM PARA O DESTINATARIO
                    salvarMensagem(idUsuarioDestinatario,idUsuarioRemetente,mensagem);

                    //SALVAR CONVERSA REMETENTE
                    Conversa conversa = new Conversa();
                    conversa.setIdUsuario(idUsuarioDestinatario);
                    conversa.setNome(nomeUsuarioDestinatario);
                    conversa.setMensagem(textoMensagem);

                    salvarConversa(idUsuarioRemetente,idUsuarioDestinatario,conversa);

                    //SALVAR CONVERSA DESTINATARIO
                    conversa = new Conversa();
                    conversa.setIdUsuario(idUsuarioRemetente);
                    conversa.setNome(nomeUsuarioRemetente);
                    conversa.setMensagem(textoMensagem);

                    salvarConversa(idUsuarioDestinatario, idUsuarioRemetente, conversa);

                    editMensagem.setText("");

                }
            }
        });

        //ESCOLHER CLIP
        btClip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ConversaActivity.this, "I'm still coding this!",Toast.LENGTH_SHORT).show();
                compartilharFoto();
            }
        });
    }
    private void compartilharFoto() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //TESTAR PROCESSO DE RETORNO DOS DADOS
        if(requestCode == 1 && resultCode == RESULT_OK && data != null){

            //RECUPERAR LOCAL DO RECURSO
            filePath = data.getData();

            //RECUPERAR DADOS USUARIOS
            //DADOS DO USUARIO LOGADO
            Preferencias preferencias = new Preferencias(ConversaActivity.this);
            //idUsuarioRemetente = preferencias.getIdentificador();
            //nomeUsuarioRemetente = preferencias.getNome();

            //DADOS DESTINATARIO
            Bundle extra = getIntent().getExtras();
            if(extra != null){
                nomeUsuarioDestinatario = extra.getString("nome");
                String emailDestinatario = extra.getString("email");
                idUsuarioDestinatario = Base64Custom.codificartBase64(emailDestinatario);
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat("ddmmaaaahhmmss");
            String nomeImagem = dateFormat.format(new Date());
            UploadTask uploadTask = imagesRef.child(idUsuarioRemetente).child(idUsuarioDestinatario).child(nomeImagem).putFile(filePath);
        }

    }

    private boolean salvarMensagem(String idRemetente, String idDestinatario, Mensagem mensagem){
        try{
            firebase = ConfiguracaoFirebase.getFirebase().child("mensagens");
            firebase.child(idRemetente).child(idDestinatario).push().setValue(mensagem);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private boolean salvarConversa(String idRemetente, String idDestinatario, Conversa conversa){
        try{
            firebase = ConfiguracaoFirebase.getFirebase().child("conversas");
            firebase.child(idRemetente).child(idDestinatario).setValue(conversa);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebase.removeEventListener(valueEventListnerMensagem);
    }
}
