package darkchat.tanz.com.darkchat.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import darkchat.tanz.com.darkchat.Helper.Base64Custom;
import darkchat.tanz.com.darkchat.Helper.Preferencias;
import darkchat.tanz.com.darkchat.R;
import darkchat.tanz.com.darkchat.config.ConfiguracaoFirebase;
import darkchat.tanz.com.darkchat.model.Usuario;

public class LoginActivity extends AppCompatActivity {

    private Button botaoLogIn;
    private EditText email;
    private EditText senha;
    private Usuario usuario;
    private FirebaseAuth autenticacao;
    private CheckBox checkBox;

    private DatabaseReference firebase;
    private String identificadorUsuarioLogado;

    //ARQUIVO EMAIL E SENHA
    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        verificarUserLogado();

        email = (EditText) findViewById(R.id.edit_login_email);
        senha = (EditText) findViewById(R.id.edit_logar_password);
        botaoLogIn = (Button) findViewById(R.id.loginButtonId);
        checkBox = (CheckBox) findViewById(R.id.CB_savePass_Id);

        botaoLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usuario = new Usuario();
                usuario.setEmail(email.getText().toString());
                usuario.setSenha(senha.getText().toString());

                SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
                SharedPreferences.Editor userSalvo = sharedPreferences.edit();
                userSalvo.putString("email", email.getText().toString());

                if(checkBox.isChecked()) {
                    userSalvo.putString("checkBox", "true");
                    userSalvo.putString("password", senha.getText().toString());
                }else{
                    userSalvo.putString("checkBox", "false");
                    userSalvo.putString("password", "");
                }

                userSalvo.commit();
                validarLogin();
            }
        });
        //RECUPERAR MAIL | PASS | CHECKBOX
        SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
        if(sharedPreferences.contains("email")){
            String emailUsuario = sharedPreferences.getString("email","");
            email.setText(emailUsuario);
        }
        if(sharedPreferences.contains("password")){
            String passwordUsuario = sharedPreferences.getString("password","");
            senha.setText(passwordUsuario);
        }
        if(sharedPreferences.contains("checkBox")){
            String checkEstado = sharedPreferences.getString("checkBox","");
            if (checkEstado == "true"){
                checkBox.setChecked(true);
            }else {
                checkBox.setChecked(false);
            }
        }
    }

    private void validarLogin(){
        autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();
        autenticacao.signInWithEmailAndPassword(usuario.getEmail(),usuario.getSenha()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){

                    Preferencias preferencias = new Preferencias(LoginActivity.this);
                    identificadorUsuarioLogado = Base64Custom.codificartBase64(usuario.getEmail());
                    preferencias.salvarDados(identificadorUsuarioLogado);

                    abrirTelaPrincipal();

                    Toast.makeText(LoginActivity.this,"Welcome" + ". I've missed you",Toast.LENGTH_SHORT).show();
                }else{
                    String erroExeption = "";
                    try{
                        throw task.getException();
                    }catch (FirebaseAuthInvalidUserException e){
                        erroExeption = "Are you sure that's your e-mail? You're not in the list!";
                    }catch (FirebaseAuthInvalidCredentialsException e) {
                        erroExeption = "HA-HA. That's not your password! Nice try hacker!";
                    }catch (Exception e) {
                        erroExeption = "Something's fishy. Please try again!";
                        e.printStackTrace();
                    }
                    Toast.makeText(LoginActivity.this, erroExeption , Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void verificarUserLogado(){
        autenticacao = ConfiguracaoFirebase.getFirebaseAutenticacao();
        if(autenticacao.getCurrentUser() != null){
            abrirTelaPrincipal();
        }
    }

    public void abrirTelaPrincipal(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void abrirCadastroUsuario(View v){
        Intent intent = new Intent(LoginActivity.this, CadastroActivity.class);
        startActivity(intent);
    }
}
