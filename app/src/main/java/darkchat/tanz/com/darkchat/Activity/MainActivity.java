package darkchat.tanz.com.darkchat.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import darkchat.tanz.com.darkchat.Helper.Base64Custom;
import darkchat.tanz.com.darkchat.Helper.Preferencias;
import darkchat.tanz.com.darkchat.R;
import darkchat.tanz.com.darkchat.adapter.ContatoAdapter;
import darkchat.tanz.com.darkchat.config.ConfiguracaoFirebase;
import darkchat.tanz.com.darkchat.model.Contato;
import darkchat.tanz.com.darkchat.model.Usuario;

public class MainActivity extends AppCompatActivity {

    public static final int RC_SIGN_IN = 1;
    private static final int RC_PHOTO_PICKER = 2;

    private Toolbar toolbar;
    private String identificadorUsuarioContato;
    private ValueEventListener valueEventListenerContatos;

    //FIREBASE
    private FirebaseAuth usuarioAutenticao;
    private DatabaseReference firebase;
    private FirebaseStorage storage;
    private StorageReference imagesRef;

    private AdView adView;
    //Contactos
    private ListView listView;
    private ArrayAdapter adapter;
    private ArrayList<Contato> contactos;
    private File imagemLocal;


    private int posicao;
    private Contato contato;
    private Contato userContato;
    NotificationCompat.Builder notification;
    private static final int uniqueID = 45612;


    @Override
    protected void onPause() {
        if(adView != null){
            adView.pause();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adView != null) {
            adView.resume();
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        firebase.addValueEventListener(valueEventListenerContatos);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebase.removeEventListener(valueEventListenerContatos);
    }

    @Override
    protected void onDestroy() {
        if(adView != null){
            adView.destroy();
        }
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //FIREBASE INICIALIZAR
        storage = FirebaseStorage.getInstance();
        imagesRef = storage.getReference();
        firebase = ConfiguracaoFirebase.getFirebase();
        usuarioAutenticao = ConfiguracaoFirebase.getFirebaseAutenticacao();


        //GET USER
        Preferencias preferencias = new Preferencias(getApplicationContext());
        String indentificadorUsuarioLogado = preferencias.getIdentificador();

        //CRIA ADS
        AdView adView = (AdView) findViewById(R.id.adBanner);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("DarkChat");
        //toolbar.setTitleTextColor();
        setSupportActionBar(toolbar);

        //*********************CRIAR LISTA CONTACTOS*********************


        contactos = new ArrayList<>();
        listView = (ListView) findViewById(R.id.lv_contatosId);

        adapter = new ContatoAdapter(getApplicationContext(), contactos);
        listView.setAdapter(adapter);

        //LONG CLICK NOS CONTACTOS TRATADO EM BAIXO DO ONCREAT
        registerForContextMenu(listView);

        //CONTACTOS DO USERLOGADO
        firebase = ConfiguracaoFirebase.getFirebase()
                .child("contatos").child(indentificadorUsuarioLogado);

        //LISTNER PARA RECUPERAR CONTACTOS
        valueEventListenerContatos = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //LIMPAR LISTA
                contactos.clear();

                //LISTAR CONTATOS
                for(DataSnapshot dados: dataSnapshot.getChildren()){
                    contato = dados.getValue(Contato.class);
                    String identificadorContato = Base64Custom.codificartBase64(contato.getEmail());
                    imagesRef = imagesRef.child("images").child("perfil").child(identificadorContato).child("perfilpic");

                    imagemLocal = null;
                    try {
                        imagemLocal = File.createTempFile("images", "jpg");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    imagesRef.getFile(imagemLocal).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                            // Local temp file has been created
                            String filepath = imagemLocal.getPath();
                            Bitmap bitmap = BitmapFactory.decodeFile(filepath);
                            contato.setImagemPerfil(bitmap);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle any errors
                        }
                    });
                    contactos.add(contato);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(MainActivity.this, ConversaActivity.class);

                //RECUPERA DADOS A SEREM PASSSADOS
                Contato contato = contactos.get(i);

                //ENVIANDO DADOS PARA CONVERSA ACTIVITY
                intent.putExtra("nome",contato.getNome());
                intent.putExtra("email",contato.getEmail());
                startActivity(intent);
            }
        });
    }

    //^^^^^^^^^^^^^^^^^^^^^^^TRATAR LONG PRESS NOS CONTACTOS^^^^^^^^^^^^^^^^^^^^^^^^^^
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contactos_long_press, menu);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        posicao = info.position;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()){
            case R.id.amigo_perfil:
                Toast.makeText(MainActivity.this,"I'm still programing this.. I only have 3 hands!",Toast.LENGTH_LONG).show();
                return true;
            case R.id.amigo_apelido:
                mudarNome();
                return true;
            case R.id.amigo_remover:
                removerInimigo();
                return true;
            case R.id.amigo_bloquear:
                bloquearInimigo();
                return true;

        }
        return super.onContextItemSelected(item);
    }
    //^^^^^^^^^^^^^^^^^^^^^^^TRATAR LONG PRESS NOS CONTACTOS^^^^^^^^^^^^^^^^^^^^^^^^^^

    //*********************CRIAR LISTA CONTACTOS*********************
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_apagarMensagens:
                apagarMensagens();
                Toast.makeText(MainActivity.this,"OH NO YOU JUST DELETED ALL OF YOUR MESSAGES! oh wait. that's what you wanted..",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.item_sair:
                apagarMensagens();
                deslogarUsuario();
                return true;
            case R.id.action_settings:
                //Toast.makeText(MainActivity.this,"Settings still under construction!",Toast.LENGTH_SHORT).show();
                abrirSettings();
                return true;
            case R.id.item_adicionar:
                abrirCadastroContato();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void abrirCadastroContato(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);

        //CONFIG DIALOG
        alertDialog.setTitle("Look at you, making friends and what not");
        alertDialog.setMessage("Does this friend of yours, have an \ne-mail?");
        alertDialog.setCancelable(false);

        final EditText editText = new EditText(MainActivity.this);
        alertDialog.setView(editText);

        //CONFIG BOTOES
        alertDialog.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String emailContacto = editText.getText().toString().toLowerCase();

                //RECUPERAR IDENTIFICADOR USUARIO LOGADO (base 64)
                Preferencias preferencias = new Preferencias(MainActivity.this);
                final String identificadorUsuarioLogado = preferencias.getIdentificador();

                //VALIDA EMAIL
                if (emailContacto.isEmpty()) {
                    Toast.makeText(MainActivity.this, "I asked kindly... GIVE ME YOUR FRIEND'S E-MAIL!", Toast.LENGTH_LONG).show();
                } else {
                    identificadorUsuarioContato = Base64Custom.codificartBase64(emailContacto);

                    firebase = ConfiguracaoFirebase.getFirebase();
                    firebase = firebase.child("bloqueado").child(identificadorUsuarioLogado).child(identificadorUsuarioContato);

                    //verificar se bloquei
                    firebase.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.getValue() != null) {
                                Toast.makeText(MainActivity.this, "I'm sorry, you've blocked this person!",Toast.LENGTH_LONG).show();
                            }else{
                                firebase = ConfiguracaoFirebase.getFirebase();
                                firebase = firebase.child("bloqueado").child(identificadorUsuarioContato).child(identificadorUsuarioLogado);
                                firebase.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.getValue() !=null){
                                            Toast.makeText(MainActivity.this, "I'm sorry, this person has blocked you!",Toast.LENGTH_LONG).show();
                                        }else{
                                            firebase = ConfiguracaoFirebase.getFirebase();
                                            firebase = ConfiguracaoFirebase.getFirebase().child("usuarios").child(identificadorUsuarioContato);
                                            firebase.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    if (dataSnapshot.getValue() != null) {
                                                        if (dataSnapshot.getValue() != null) {

                                                            //RECUPERAR DADOS CONTATO A SER ADICIONADO
                                                            Usuario usuarioContato = dataSnapshot.getValue(Usuario.class);

                                                            firebase = ConfiguracaoFirebase.getFirebase();
                                                            firebase = firebase.child("contatos").child(identificadorUsuarioLogado).child(identificadorUsuarioContato);

                                                            Contato meuContato = new Contato();
                                                            meuContato.setIndentificadorUsuario(identificadorUsuarioContato);
                                                            meuContato.setEmail(usuarioContato.getEmail());
                                                            meuContato.setNome(usuarioContato.getNome());
                                                            firebase.setValue(meuContato);

                                                            //CONTACTO OPOSTO
                                                            firebase = ConfiguracaoFirebase.getFirebase().child("usuarios").child(identificadorUsuarioLogado);
                                                            firebase.addValueEventListener(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                                    Usuario usuarioAtual = dataSnapshot.getValue(Usuario.class);
                                                                    firebase = ConfiguracaoFirebase.getFirebase();
                                                                    firebase = firebase.child("contatos").child(identificadorUsuarioContato).child(identificadorUsuarioLogado);

                                                                    Contato teuContato = new Contato();
                                                                    teuContato.setIndentificadorUsuario(identificadorUsuarioLogado);
                                                                    teuContato.setEmail(usuarioAtual.getEmail());
                                                                    teuContato.setNome(usuarioAtual.getNome());
                                                                    firebase.setValue(teuContato);
                                                                }

                                                                @Override
                                                                public void onCancelled(DatabaseError databaseError) {

                                                                }
                                                            });
                                                        } else {
                                                            Toast.makeText(MainActivity.this, "That e-mail is not cool enough to be in our list", Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }
        });

        alertDialog.setNegativeButton("GO BACK!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        alertDialog.create();
        alertDialog.show();
    }

    private void deslogarUsuario(){
        usuarioAutenticao.signOut();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void abrirSettings(){
        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
        startActivity(intent);
    }

    private boolean apagarMensagens(){
        try{
            String userAtual = Base64Custom.codificartBase64(usuarioAutenticao.getCurrentUser().getEmail().toString());
            firebase = ConfiguracaoFirebase.getFirebase().child("mensagens").child(userAtual);
            firebase.setValue(null);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private boolean removerInimigo(){
        try{
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
            //CONFIG DA DIALOG
            alertDialog.setTitle("Oh no you didn't");
            alertDialog.setMessage("I don't like this person either.. but this might mean the end of human race! Are you sure you want to go through with this?");
            alertDialog.setCancelable(false);

            Preferencias preferencias = new Preferencias(MainActivity.this);
            final String identificadorUsuarioLogado = preferencias.getIdentificador();

            userContato = new Contato();
            userContato = contactos.get(posicao);
            identificadorUsuarioContato = userContato.getIndentificadorUsuario();

            alertDialog.setPositiveButton("DO IT!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    firebase = ConfiguracaoFirebase.getFirebase();
                    firebase = firebase.child("contatos").child(identificadorUsuarioLogado).child(identificadorUsuarioContato);
                    firebase.removeValue();
                    firebase = ConfiguracaoFirebase.getFirebase();
                    firebase = firebase.child("contatos").child(identificadorUsuarioContato).child(identificadorUsuarioLogado);
                    firebase.removeValue();
                }
            });
            alertDialog.setNegativeButton("Just take me back..", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //SAI SO
                }
            });
            alertDialog.create();
            alertDialog.show();

            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
    private boolean mudarNome(){
        try{
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
            //CONFIG DA DIALOG
            alertDialog.setTitle("Change Nickname");
            alertDialog.setMessage("I don't like this person's name either.. LET'S CHANGE IT! what should the new name be?");
            alertDialog.setCancelable(false);
            final EditText nomeDigitado = new EditText(MainActivity.this);
            alertDialog.setView(nomeDigitado);

            Preferencias preferencias = new Preferencias(MainActivity.this);
            final String identificadorUsuarioLogado = preferencias.getIdentificador();

            userContato = new Contato();
            userContato = contactos.get(posicao);
            identificadorUsuarioContato = userContato.getIndentificadorUsuario();

            //BOTOES
            alertDialog.setPositiveButton("CHANGE IT!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    userContato.setNome(nomeDigitado.getText().toString());
                    firebase = ConfiguracaoFirebase.getFirebase();
                    firebase = firebase.child("contatos").child(identificadorUsuarioLogado).child(identificadorUsuarioContato);
                    firebase.setValue(userContato);
                }
            });

            alertDialog.setNegativeButton("I like the old one..", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //SAI SO
                }
            });

            alertDialog.create();
            alertDialog.show();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private boolean bloquearInimigo(){
        try{
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
            //CONFIG DA DIALOG
            alertDialog.setTitle("BLOCK USER!");
            alertDialog.setMessage("I don't like this person either, but if you go through with this you might not be able to live with yourself..");
            alertDialog.setCancelable(false);

            Preferencias preferencias = new Preferencias(MainActivity.this);
            final String identificadorUsuarioLogado = preferencias.getIdentificador();

            userContato = new Contato();
            userContato = contactos.get(posicao);
            identificadorUsuarioContato = userContato.getIndentificadorUsuario();

            //BOTOES
            alertDialog.setPositiveButton("BLOCK!", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //REGISTAR COMO BLOQUEADO
                    firebase = ConfiguracaoFirebase.getFirebase();
                    firebase = firebase.child("bloqueado").child(identificadorUsuarioLogado).child(identificadorUsuarioContato);
                    firebase.setValue(userContato);
                    //REMOVER
                    firebase = ConfiguracaoFirebase.getFirebase();
                    firebase = firebase.child("contatos").child(identificadorUsuarioLogado).child(identificadorUsuarioContato);
                    firebase.removeValue();
                    firebase = ConfiguracaoFirebase.getFirebase();
                    firebase = firebase.child("contatos").child(identificadorUsuarioContato).child(identificadorUsuarioLogado);
                    firebase.removeValue();

                }
            });

            alertDialog.setNegativeButton("Just take me back..", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //SAI SO
                }
            });

            alertDialog.create();
            alertDialog.show();
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == RC_SIGN_IN){
            if(resultCode == RESULT_OK){
                // Sign-in succeeded, set up the UI
                Toast.makeText(this,"Signed in",Toast.LENGTH_SHORT).show();
            }else if(resultCode == RESULT_CANCELED){
                // Sign in was canceled by the user, finish the activity
                Toast.makeText(this,"Sign in canceled",Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }
}


