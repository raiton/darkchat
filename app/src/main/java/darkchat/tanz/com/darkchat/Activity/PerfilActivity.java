package darkchat.tanz.com.darkchat.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.FileNotFoundException;
import java.io.IOException;

import darkchat.tanz.com.darkchat.Helper.Base64Custom;
import darkchat.tanz.com.darkchat.Helper.Preferencias;
import darkchat.tanz.com.darkchat.R;
import darkchat.tanz.com.darkchat.model.Perfil;
import darkchat.tanz.com.darkchat.model.Usuario;

public class PerfilActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private FloatingActionButton botaoImagem;
    private final int PICK_IMAGE_REQUEST = 72;
    private Uri filePath;
    private ImageView fotoUser;

    private FirebaseStorage storage;
    private StorageReference imagesRef;
    private StorageReference load;

    private String identificadorUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        //FIREBASE INICIALIZAR
        storage = FirebaseStorage.getInstance();
        imagesRef = storage.getReference();

        //GetUser
        Preferencias preferencias = new Preferencias(PerfilActivity.this);
        identificadorUsuario = preferencias.getIdentificador();

        toolbar = findViewById(R.id.tb_perfil);
        botaoImagem = findViewById(R.id.botaoPerfilImagem);
        fotoUser = findViewById(R.id.imagemPerfil);

        imagesRef = imagesRef.child("images").child("perfil").child(identificadorUsuario).child("perfilpic");
        imagesRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.with(getApplicationContext()).load(uri.toString()).into(fotoUser);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });

        //CONFIGURAR TOOLBAR
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_left);
        toolbar.setTitle("Profile");
        setSupportActionBar(toolbar);

        botaoImagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escolherImagem();
            }
        });
    }

    private void escolherImagem(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null &&data.getData()!= null){
            filePath = data.getData();
            try{
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),filePath);
                fotoUser.setImageBitmap(bitmap);
                uploadImagem();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private void uploadImagem(){
        if(filePath!=null){
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading profile picture...");
            progressDialog.show();
            imagesRef = storage.getReference();
            StorageReference ref = imagesRef.child("images").child("perfil").child(identificadorUsuario).child("perfilpic");
            ref.putFile(filePath).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    progressDialog.dismiss();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(PerfilActivity.this,"Upload failed, Don't worry you can upload a picture later!",Toast.LENGTH_LONG).show();
                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                    double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot.getTotalByteCount());
                    progressDialog.setMessage("Uploaded "+(int)progress+"%");
                }
            });
        }
    }
}
