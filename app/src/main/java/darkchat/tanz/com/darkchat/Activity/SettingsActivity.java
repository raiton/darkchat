package darkchat.tanz.com.darkchat.Activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import darkchat.tanz.com.darkchat.Helper.Base64Custom;
import darkchat.tanz.com.darkchat.Helper.Preferencias;
import darkchat.tanz.com.darkchat.R;
import darkchat.tanz.com.darkchat.adapter.perfilAdapter;
import darkchat.tanz.com.darkchat.model.Perfil;


public class SettingsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ListView myListView;
    private ListView perfil;

    private perfilAdapter adapterperfil;
    private String idUsuario;
    private String emailUsuario;
    private FirebaseStorage storage;
    private StorageReference imagemRef;
    private StorageReference storageRef;
    private ImageView imagemperfil;

    private String[] nomes = {"Themes", "Notifications", "Help"};
    private int[] images = {R.drawable.ic_color_lens, R.drawable.ic_notifications_active, R.drawable.ic_help};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Preferencias preferencias = new Preferencias(SettingsActivity.this);
        String identificadorContato = preferencias.getIdentificador();

        //FIREBASE
        storage = FirebaseStorage.getInstance();
        storageRef = storage.getReference();
        imagemRef = storageRef.child("images").child("perfil").child(identificadorContato).child("perfilpic");


        toolbar = findViewById(R.id.tb_settings);
        //CONFIGURAR TOOLBAR
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_left);
        toolbar.setTitle("Settings");
        setSupportActionBar(toolbar);

        perfil = findViewById(R.id.perfil_settings);
        myListView = findViewById(R.id.lv_settings);

        //PERFIL
        PerfilAdapter adapterperfil = new PerfilAdapter();
        perfil.setAdapter(adapterperfil);
        perfil.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(SettingsActivity.this, PerfilActivity.class);
                startActivity(intent);
            }
        });

        //SETTINGS
        DefinicoesAdapter adapter = new DefinicoesAdapter();
        myListView.setAdapter(adapter);

        myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String nome = nomes[i];
                switch (nome){
                    case "Themes":
                        Intent intent = new Intent(SettingsActivity.this, TemasActivity.class);
                        startActivity(intent);
                        //Toast.makeText(SettingsActivity.this,"Still Coding Themes",Toast.LENGTH_SHORT).show();
                        break;
                    case "Notifications":
                        Toast.makeText(SettingsActivity.this,"Still Coding Notifications",Toast.LENGTH_SHORT).show();
                        break;
                    case "Help":
                        Toast.makeText(SettingsActivity.this,"Still Coding Help",Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });


    }

    class DefinicoesAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.lista_settings, null);

            ImageView imageView = view.findViewById(R.id.settings_imagem);
            TextView textView = view.findViewById(R.id.settings_nome);

            imageView.setImageResource(images[i]);
            textView.setText(nomes[i]);

            return view;
        }
    }

    class PerfilAdapter extends BaseAdapter {
        private File imagemLocal;

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.customperfil, null);
            imagemperfil = view.findViewById(R.id.settings_imagemPerfil);
            TextView emailPerfil = view.findViewById(R.id.settings_textoPerfil);

            //GETEMAIL
            Preferencias preferencias = new Preferencias(SettingsActivity.this);
            idUsuario = preferencias.getIdentificador();
            emailUsuario = Base64Custom.decodificarBase64(idUsuario);
            emailPerfil.setText(emailUsuario);

            //GETIMAGEM
            imagemLocal = null;
            try {
                imagemLocal = File.createTempFile("images", "jpg");
            } catch (IOException e) {
                e.printStackTrace();
            }

            imagemRef.getFile(imagemLocal).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    String filepath = imagemLocal.getPath();
                    Bitmap bitmap = BitmapFactory.decodeFile(filepath);
                    imagemperfil.setImageBitmap(bitmap);
                }
            });
            return view;
        }
    }
}


