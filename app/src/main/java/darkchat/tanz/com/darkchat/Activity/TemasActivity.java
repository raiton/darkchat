package darkchat.tanz.com.darkchat.Activity;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import darkchat.tanz.com.darkchat.R;

public class TemasActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RadioGroup radioGroup;
    private RadioButton botaoSelecionado;
    private Button botaoTema;
    private RelativeLayout layout;
    private static final String ARQUIVO_PREFERENCIA = "ArqPreferencia";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        toolbar = (Toolbar) findViewById(R.id.tb_settings);
        radioGroup = findViewById(R.id.radioCoresGroupId);
        botaoTema = findViewById(R.id.botaoSalvarTemaId);
        layout = findViewById(R.id.layoutId);

        //CONFIGURAR TOOLBAR
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_left);
        toolbar.setTitle("Themes");
        setSupportActionBar(toolbar);


        botaoTema.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int idSelecionado = radioGroup.getCheckedRadioButtonId();
                if(idSelecionado > 0){
                    botaoSelecionado = findViewById(idSelecionado);
                    SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA,0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    String corEscolhida = botaoSelecionado.getText().toString();
                    editor.putString("corEscolhida", corEscolhida);
                    editor.commit();
                    //setTema(corEscolhida);
                    Toast.makeText(TemasActivity.this,"Still Coding Themes!",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    /*private void setTema(String tema){
        if(tema.equals("DarkChat")){

        }else if(tema.equals("LightChat")){

        }*/
    }

