package darkchat.tanz.com.darkchat.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import darkchat.tanz.com.darkchat.R;
import darkchat.tanz.com.darkchat.model.Contato;

/**
 * Created by RaitonGG on 31/08/2017.
 */

public class ContatoAdapter extends ArrayAdapter<Contato>{

    private Context context;
    private ArrayList<Contato> contatos;

    public ContatoAdapter(Context c, ArrayList<Contato> objects) {
        super(c, 0, objects);
        this.context = c;
        this.contatos = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        //VERIFICA SE A LISTA ESTÁ VAZIA
        if(contatos != null){
            //INICIALIZAR O OBJECTO PARA MONTAGEM DA VIEW
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

            //MONTA VIEW A PARTIR DO XML
            view = inflater.inflate(R.layout.lista_contato, parent,false);

            //RECUPERA ELEMENTO PARA EXIBICAO
            TextView nomeContato = view.findViewById(R.id.tv_nome);
            TextView emailContato = view.findViewById(R.id.tv_subtitulo);
            ImageView imagemContato = view.findViewById(R.id.imagemContatoId);

            Contato contato = contatos.get(position);
            nomeContato.setText(contato.getNome());
            emailContato.setText(contato.getEmail());
            imagemContato.setImageBitmap(contato.getImagemPerfil());
        }

        return view;
    }
}
