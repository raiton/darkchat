package darkchat.tanz.com.darkchat.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import darkchat.tanz.com.darkchat.Helper.Preferencias;
import darkchat.tanz.com.darkchat.R;
import darkchat.tanz.com.darkchat.model.Mensagem;

/**
 * Created by RaitonGG on 21/08/2017.
 */
public class MensagemAdapter extends ArrayAdapter<Mensagem> {

    private Context context;
    private ArrayList<Mensagem> mensagens;

    public MensagemAdapter(Context c, ArrayList<Mensagem> objects) {
        super(c, 0, objects);
        this.context=c;
        this.mensagens=objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        //VERIFICA SE LISTA ESTÁ PREENCHIDA
        if(mensagens != null){
            //RECUPERA DADOS DO USUARIO REMETENTE
            Preferencias preferencias = new Preferencias(context);
            String idUsuarioRemetente = preferencias.getIdentificador();

            //INICIALIZA OBJECTO PARA MONTAGEM DO LAYOUT
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

            //RECUPERA MENSAGEM
            Mensagem mensagem = mensagens.get(position);

            //MONTA VIEW A PARTIR DO XML
            if(idUsuarioRemetente.equals(mensagem.getIdUsuario())){
                view = inflater.inflate(R.layout.item_mensagem_direita,parent,false);
            }else{
                view = inflater.inflate(R.layout.item_mensagem_esquerda,parent,false);
            }

            //RECUPERA ELEMENTO PARA EXIBICAO
            TextView textoMensagem = (TextView) view.findViewById(R.id.tv_mensagem);
            textoMensagem.setText(mensagem.getMensagem());
        }

        return view;
    }
}
