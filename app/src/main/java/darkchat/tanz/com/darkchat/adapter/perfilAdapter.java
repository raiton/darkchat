package darkchat.tanz.com.darkchat.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import darkchat.tanz.com.darkchat.R;
import darkchat.tanz.com.darkchat.model.Perfil;

/**
 * Created by RaitonGG on 05/09/2017.
 */

public class perfilAdapter extends RecyclerView.Adapter<perfilAdapter.MyViewHolder>{

    private LayoutInflater inflater;
    private List<Perfil> data = Collections.emptyList();

    public perfilAdapter(Context context, List<Perfil> data){
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.customperfil,parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Perfil current = data.get(position);
        holder.email.setText(current.getEmail());
        holder.imagem.setImageResource(current.getIconId());
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView email;
        ImageView imagem;

        public MyViewHolder(View itemView) {
            super(itemView);
            email = itemView.findViewById(R.id.settings_textoPerfil);
            imagem = itemView.findViewById(R.id.settings_imagemPerfil);
        }
    }
}
