package darkchat.tanz.com.darkchat.model;

import android.graphics.Bitmap;

/**
 * Created by RaitonGG on 19/08/2017.
 */
public class Contato {

    private String indentificadorUsuario;
    private String nome;
    private String email;
    private Bitmap imagemPerfil;

    public Contato() {
    }

    public String getIndentificadorUsuario() {
        return indentificadorUsuario;
    }

    public void setIndentificadorUsuario(String indentificadorUsuario) {
        this.indentificadorUsuario = indentificadorUsuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Bitmap getImagemPerfil() {
        return imagemPerfil;
    }

    public void setImagemPerfil(Bitmap imagemPerfil) {
        this.imagemPerfil = imagemPerfil;
    }
}
