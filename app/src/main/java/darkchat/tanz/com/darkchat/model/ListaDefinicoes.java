package darkchat.tanz.com.darkchat.model;

/**
 * Created by RaitonGG on 02/09/2017.
 */

public class ListaDefinicoes {
    //private String nome;
    private int image_id;
    private String texto;

    public ListaDefinicoes(int image_id, String texto){
        this.image_id = image_id;
        this.texto = texto;
    }

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
}
