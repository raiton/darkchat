package darkchat.tanz.com.darkchat.model;

/**
 * Created by RaitonGG on 05/09/2017.
 */

public class Perfil {
    int iconId;
    String email;
    String estado;

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
